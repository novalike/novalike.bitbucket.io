//
// FattoreMamma Network Release 1.8
//
var fMn = document; // shortcut
var cssId = 'myCss'; // you could encode the css path itself to generate id..
if (!fMn.getElementById(cssId)) {
	var head = fMn.getElementsByTagName('head')[0];
	var link = fMn.createElement('link');
	link.id = cssId;
	link.rel = 'stylesheet';
	link.type = 'text/css';
	link.href = 'fmbar.css';
	link.media = 'all';
	head.appendChild(link);
}

// Network Bar
document
		.write('<div class="top_banner" id="fmNetwork"><div style="background:none;height:35px;width:970px;margin:0;margin-left:auto;margin-right:auto;text-align:left;/*border: 1px solid #ff0000*/"><a style="background:#92BD27;margin:0;text-decoration:none;line-height:0;font-size:0;" href="http://fattoremamma.com/network/" title="FattoreMamma Network" target="_blank"><img style="margin:0;padding:0;border:0" src="http://adv.fattoremamma.com/fmn.png" alt="FattoreMamma Network" height="35" width="300" border="0"></a></div></div></div>');
document
		.write('<style type="text/css">.top_banner{z-index:99999;} #dotnAdDivSkLb{margin-top:35px;}</style>');

/* <script type="text/javascript">GA_googleFillSlot("FM_300x29_expandable");</script> */

// Tiscali <script>
function fmNetwork() {
	var fmBeh = document.createElement('script');
	fmBeh.type = "text/javascript";
	fmBeh.src = "http://js.revsci.net/gateway/gw.js?csid=F11190&auto=t";
	var b = document.body;
	b.appendChild(fmBeh, b.lastChild);
}

// Tiscali Test Check
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
			function(m, key, value) {
				vars[key] = value;
			});
	return vars;
}

var tiscaliTest = getUrlVars()["tiscaliTest"];

if (tiscaliTest) {
	webSite = dotnAd.brand;
	dotnAd.setTBrand("adv");
	dotnAd.setTChannel("test");
	dotnAd.setTSection(webSite);
	dotnAd.setTSubSection("");
	dotnAd.setFunGen("adv");
	try {
		dotnAd.setKeywords(dotnKeyword);
	} catch (e) {
	}
	dotnAd.callAdserver();
}

// DOM Ready Check
var arFlag = 0;

if (document.addEventListener)
	document.addEventListener("DOMContentLoaded", function() {
		arFlag = 1;
		fmNetwork()
	}, false)
else if (document.all && !window.opera) {
	document
			.write('<script type="text/javascript" id="clTag" defer="defer" src="javascript:void(0)"><\/script>')
	var clTag = document.getElementById("clTag")
	clTag.onreadystatechange = function() {
		if (this.readyState == "complete") {
			arFlag = 1
			fmNetwork()
		}
	}
}

//Imposto il background esistente a centrato e a 35px dal top
document.body.style.backgroundPosition = "center 35px";

window.onload = function() {

	var test = document.getElementById("dotnAdDivSkLb");
	var styles;
	if (test) {
		styles = '#fmNetwork{position:fixed;}'
	} else {
		styles = '#fmNetwork{position:relative;}'
	}
	var css = document.createElement('style');
	css.type = 'text/css';
	if (css.styleSheet)
		css.styleSheet.cssText = styles;
	else
		css.appendChild(document.createTextNode(styles));
	document.getElementsByTagName("head")[0].appendChild(css);

	// Mi assicuro che il background sia centrato e a 35px dal top
	document.body.style.backgroundPosition = "center 35px";

	setTimeout("if(!arFlag) {fmNetwork()}", 0)
}
